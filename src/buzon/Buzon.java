/*1- Define a clase Buzon para xestionar unbuzon de correo  electrónicos ordenados segundo a orde de chegada.
Para representar o buzd) String amosaPrimerNoLeido(), que amostra o primeiro correo non lido
on de correo úsase un array de correos electrónicos; estes, á súa vez, son obxectos da clase Correo e teñen como atributos un String, co contido do correo, e un indicativo para saber se foron lidos ou non.
Define a clase Correo cos métodos que creas convenientes, sabendo que a clase Buzon debe incluír os seguintes métodos públicos:
a) int numeroDeCorreos(), que calcula cantos correos hai no buzon de correo
b) void engade (Correo c), que engade c ao buzon
c) boolean porLer(), que determina se quedan correos por ler
e) String amosa(int k), que amostra o correo k-ésimo, fora lido ou non
f) void elimina(int k), que elimina o correo k-ésimo.
*/
package buzon;

import java.util.ArrayList;


public class Buzon {

    
    public static void main(String[] args) {
       ArrayList< Correo>correos = new ArrayList<Correo>();
       Correo obx= new Correo();
       for(int i=0;i<5;i++){
           obx.engade(correos);
       }
       obx.verTodos(correos);
       obx.amosa(1, correos);
        System.out.println("1º non lido :" + obx.amosaPrimerNonLeido(correos));
       obx.amosa(3, correos);
       obx.eliminar(3, correos);
       obx.verTodos(correos);
}
}