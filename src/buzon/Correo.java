package buzon;

import java.util.ArrayList;
import javax.swing.JOptionPane;


public class Correo {    
    static int totalCorreos =0; 
    private String texto;
    private int lido; // 1-> lido ; 0 --> non lido
    public Correo(){
    }
    
    //obxecto da clase Correo e teñen como atributos un String, co contido do correo, e un indicativo para saber se foron lidos ou non.
    public Correo(String te){
        texto = te;
    
    }   
    
    public String getTexto() {
        return texto;
    }   
    
    public void setTexto(String texto) {
        this.texto = texto;
    }    
    
    public int getLido() {
        return lido;
    }   
    public void setLido(int lido) {
        this.lido = lido;
    }
    
    public Correo get(int i){
        return this;
    }
    
    //void engade (Correo c), que engade c ao buzon
    public void engade(ArrayList<Correo> correos){
        String mensaxe = JOptionPane.showInputDialog("escribe mensaxe:");
        correos.add(new Correo(mensaxe));
    }
    
    //int numeroDeCorreos(), que calcula cantos correos hai no buzon de correo
    public int numeroCorreos(){
       return totalCorreos;
   }
   
    //boolean porLer(), que determina se quedan correos por ler
    public boolean porLer(ArrayList <Correo>correos){
       boolean leidos=true;// estan todos leidos
       for(Correo co :correos){
           if(co.lido== 0)
               leidos =false;
       }
       return leidos;       
   }
   
    //Para representar o buzon
   public String amosaPrimerNonLeido(ArrayList<Correo>correos){
     String res="";
       for(Correo co:correos){
         if(co.lido== 0){
             res = co.getTexto();
             break;
         }
       }  
       return res;   
   }
   
   //void elimina(int k), que elimina o correo k-ésimo.
   public void eliminar(int k,ArrayList <Correo>correos){
       correos.remove(k-1);
       System.out.println("mensaxe "+ k + "eliminado");
   }
   
   //String amosa(int k), que amostra o correo k-ésimo, fora lido ou non
   public void amosa(int k,ArrayList<Correo>correos){
       for(int i =0;i<correos.size();i++){
           if(i== k){
               System.out.println("correo :."+ correos.get(i-1).getTexto());
               correos.get(i-1).setLido(1);      
           }
   }
   } 
   
   public void verTodos(ArrayList<Correo>correos){
       for(Correo co:correos){
       System.out.println(" mensaxe "+ co.getTexto()+ " lido :" + co.getLido());
       }    
   }
}
